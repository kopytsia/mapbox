package com.example.mapbox.jobs;

import com.example.mapbox.model.Tileset;
import com.example.mapbox.service.MapboxService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.core.SchedulerLock;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
@Slf4j
@RequiredArgsConstructor
public class ScheduledTasks {
    private static final int LENGTH = 32;
    private final MapboxService mapboxService;

    @Scheduled(cron = "0 0/1 * * * ?")
    @SchedulerLock(name = "ScheduledTasks_replaceTilesetSourceTask",
            lockAtLeastForString = "PT50S", lockAtMostForString = "PT1M")
    public void replaceTilesetSourceTask() {
        try {
            File file = mapboxService.getLastBackupsAsLineDelimitedGeoJsonFile();
            String id = RandomStringUtils.randomAlphabetic(LENGTH);
            Tileset tileset = mapboxService.createTileset(id);
            mapboxService.createSource(id, file);
            mapboxService.updateTileset(tileset);
            mapboxService.deleteOldSources(id);
            mapboxService.publishTileset();
        } catch (IOException e) {
            log.error("Can`t process a file!");
        }
    }
}
