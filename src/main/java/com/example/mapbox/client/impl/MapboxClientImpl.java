package com.example.mapbox.client.impl;

import com.example.mapbox.client.MapboxClient;
import com.example.mapbox.config.MapboxConfig;
import com.example.mapbox.model.Recipe;
import com.example.mapbox.model.Tileset;
import com.example.mapbox.model.TilesetResponseBody;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;

@Service
@RequiredArgsConstructor
public class MapboxClientImpl implements MapboxClient {
    private final RestTemplate restTemplate;
    private final MapboxConfig mapboxConfig;

    @Override
    public HttpStatus replaceTilesetSource(File file, String id) {
        String url = buildUrl(id, mapboxConfig.getSourceReplaceUrl());
        HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = getLinkedMultiValueMapHttpEntity(file);
        ResponseEntity<Void> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, requestEntity, Void.class);
        return responseEntity.getStatusCode();
    }

    @Override
    public HttpStatus createTileset(Tileset tileset) {
        String url = buildUrl(tileset.getName(), mapboxConfig.getTilesetCreateUrl());
        HttpEntity<Tileset> httpEntity = getTilesetHttpEntity(tileset);
        ResponseEntity<Void> responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, Void.class);
        return responseEntity.getStatusCode();
    }

    @Override
    public HttpStatus publishTileset(String id) {
        String url = buildUrl(id, mapboxConfig.getTilesetPublishUrl());
        ResponseEntity<Void> responseEntity = restTemplate.exchange(url, HttpMethod.POST, null, Void.class);
        return responseEntity.getStatusCode();
    }

    @Override
    public HttpStatus createTilesetSource(String randomId, File file) {
        String url = buildUrl(randomId, mapboxConfig.getSourceCreateUrl());
        HttpEntity<LinkedMultiValueMap<String, Object>> httpEntity = getLinkedMultiValueMapHttpEntity(file);
        ResponseEntity<Void> responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, Void.class);
        return responseEntity.getStatusCode();
    }

    @Override
    public HttpStatus updateTileset(Tileset tileset) {
        String url = buildUrl(tileset.getName(), mapboxConfig.getTilesetUpdateUrl());
        HttpEntity<Recipe> httpEntity = getRecipeHttpEntity(tileset);
        ResponseEntity<Void> responseEntity = restTemplate.exchange(url, HttpMethod.PATCH, httpEntity, Void.class);
        return responseEntity.getStatusCode();
    }

    @Override
    public ResponseEntity<TilesetResponseBody[]> getTilesets() {
        String listSourcesUrl = String.format(mapboxConfig.getSourceListUrl(), mapboxConfig.getMapboxUsername(), mapboxConfig.getMapboxSkToken());
        return restTemplate.exchange(listSourcesUrl, HttpMethod.GET, null, TilesetResponseBody[].class);
    }

    @Override
    public HttpStatus deleteSource(String id) {
        String url = buildUrl(id, mapboxConfig.getSourceDeleteUrl());
        ResponseEntity<Void> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, null, Void.class);
        return responseEntity.getStatusCode();
    }

    private HttpEntity<Recipe> getRecipeHttpEntity(Tileset tileset) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(tileset.getRecipe(), headers);
    }

    private String buildUrl(String id, String urlTemplate) {
        return String.format(urlTemplate, mapboxConfig.getMapboxUsername(), id, mapboxConfig.getMapboxSkToken());
    }

    private HttpEntity<LinkedMultiValueMap<String, Object>> getLinkedMultiValueMapHttpEntity(File file) {
        HttpHeaders headers = new HttpHeaders();
        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("file", new FileSystemResource(file));
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        return new HttpEntity<>(map, headers);
    }

    private HttpEntity<Tileset> getTilesetHttpEntity(Tileset tileset) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(tileset, headers);
    }
}