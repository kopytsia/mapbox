package com.example.mapbox.client;

import com.example.mapbox.model.Tileset;
import com.example.mapbox.model.TilesetResponseBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.File;

public interface MapboxClient {
    HttpStatus replaceTilesetSource(File file, String id);

    HttpStatus createTileset(Tileset tileset);

    HttpStatus publishTileset(String id);

    HttpStatus createTilesetSource(String id, File file);

    HttpStatus updateTileset(Tileset tileset);

    ResponseEntity<TilesetResponseBody[]> getTilesets();

    HttpStatus deleteSource(String id);
}
