package com.example.mapbox.model;

import lombok.Value;

@Value
public class MapboxToken {
    String token;
}
