package com.example.mapbox.model;

import lombok.Value;

@Value
public class TilesetSource {
    String source;
    int minzoom = 0;
    int maxzoom = 16;
}
