package com.example.mapbox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.time.LocalDateTime;

@Value
public class BackupInfo {
    LocalDateTime creationDate;
    String filename;
    BackupGeometryType type;

    @JsonCreator
    public BackupInfo(
            @JsonProperty("creationDate") LocalDateTime creationDate,
            @JsonProperty("filename") String filename,
            @JsonProperty("type") BackupGeometryType type
    ) {
        this.creationDate = creationDate;
        this.filename = filename;
        this.type = type;
    }
}