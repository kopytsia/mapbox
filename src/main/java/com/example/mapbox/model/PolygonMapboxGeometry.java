package com.example.mapbox.model;

import lombok.Value;

import java.util.List;

@Value
public class PolygonMapboxGeometry implements MapboxGeometry {
    String type = "Polygon";
    List<List<List<Double>>> coordinates;
}
