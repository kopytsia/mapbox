package com.example.mapbox.model;

public enum BackupGeometryType {
    SEGMENT, POLYGON
}
