package com.example.mapbox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

@Value
public class Polygon {
    long id;
    double square;
    List<List<List<Double>>> coordinates;

    @JsonCreator
    public Polygon(
            @JsonProperty("id") long id,
            @JsonProperty("square") double square,
            @JsonProperty("coordinates") List<List<List<Double>>> coordinates
    ) {
        this.id = id;
        this.square = square;
        this.coordinates = coordinates;
    }
}
