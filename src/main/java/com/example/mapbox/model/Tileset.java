package com.example.mapbox.model;

import lombok.Value;

@Value
public class Tileset {
    Recipe recipe;
    String name;
}
