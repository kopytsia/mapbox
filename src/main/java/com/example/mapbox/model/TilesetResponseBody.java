package com.example.mapbox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class TilesetResponseBody {
    String id;
    Integer size;
    Integer files;

    @JsonCreator
    public TilesetResponseBody(
            @JsonProperty("id") String id,
            @JsonProperty("size") Integer size,
            @JsonProperty("files") Integer files
    ) {
        this.id = id;
        this.size = size;
        this.files = files;
    }
}
