package com.example.mapbox.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

@Value
public class Segment {
    long id;
    double length;
    List<List<Double>> coordinates;
    String color;

    @JsonCreator
    public Segment(
            @JsonProperty("id") long id,
            @JsonProperty("length") double length,
            @JsonProperty("coordinates") List<List<Double>> coordinates,
            @JsonProperty("color") String color
    ) {
        this.id = id;
        this.length = length;
        this.coordinates = coordinates;
        this.color = color;
    }
}
