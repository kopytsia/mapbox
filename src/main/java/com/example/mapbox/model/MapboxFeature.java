package com.example.mapbox.model;

import lombok.Value;

import java.util.Map;

@Value
public class MapboxFeature {
    String type = "Feature";
    MapboxGeometry geometry;
    Map<String, Object> properties;
}
