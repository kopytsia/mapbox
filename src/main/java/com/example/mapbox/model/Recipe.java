package com.example.mapbox.model;

import lombok.Value;

import java.util.Map;

@Value
public class Recipe {
    int version = 1;
    Map<String, TilesetSource> layers;
}
