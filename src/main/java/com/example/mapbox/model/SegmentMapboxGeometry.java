package com.example.mapbox.model;

import lombok.Value;

import java.util.List;

@Value
public class SegmentMapboxGeometry implements MapboxGeometry {
    String type = "LineString";
    List<List<Double>> coordinates;
}
