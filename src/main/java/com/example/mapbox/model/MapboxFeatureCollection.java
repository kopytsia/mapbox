package com.example.mapbox.model;

import lombok.Value;

import java.util.List;

@Value
public class MapboxFeatureCollection {
    String type = "FeatureCollection";
    List<MapboxFeature> features;
}
