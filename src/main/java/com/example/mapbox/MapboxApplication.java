package com.example.mapbox;

import com.example.mapbox.config.MapboxConfig;
import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties(MapboxConfig.class)
@EnableSchedulerLock(defaultLockAtMostFor = "PT1M")
public class MapboxApplication {
	public static void main(String[] args) {
		SpringApplication.run(MapboxApplication.class, args);
	}
}
