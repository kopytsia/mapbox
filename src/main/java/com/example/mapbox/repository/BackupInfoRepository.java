package com.example.mapbox.repository;

import com.example.mapbox.model.BackupGeometryType;
import com.example.mapbox.model.BackupInfo;

import java.util.List;

public interface BackupInfoRepository {
    int create(String filename, BackupGeometryType geometryType);

    List<BackupInfo> findAll();
}
