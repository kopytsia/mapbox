package com.example.mapbox.repository;

import com.example.mapbox.model.Segment;

import java.util.List;

public interface SegmentRepository {
    int save(List<List<Double>> coordinates);

    List<Segment> findAll();

    int saveAll(List<Segment> segments);

    int deleteAll();

    boolean isSegmentsExists();
}
