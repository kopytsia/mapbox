package com.example.mapbox.repository;

import com.example.mapbox.model.Polygon;

import java.util.List;

public interface PolygonRepository {
    int save(List<List<List<Double>>> coordinates);

    List<Polygon> findAll();

    int saveAll(List<Polygon> polygons);

    int deleteAll();

    boolean isPolygonsExists();
}
