package com.example.mapbox.repository;

import com.example.mapbox.model.BackupGeometryType;
import com.example.mapbox.model.BackupInfo;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface BackupInfoRedisRepository {
    void create(BackupInfo backupInfo);

    List<BackupInfo> findAll();

    List<BackupInfo> findAllByDate(LocalDate date);

    Optional<BackupInfo> getLastByType(BackupGeometryType type);
}
