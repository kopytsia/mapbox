package com.example.mapbox.repository.impl;

import com.example.mapbox.model.Polygon;
import com.example.mapbox.model.db.tables.records.PolygonRecord;
import com.example.mapbox.repository.PolygonRepository;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.InsertReturningStep;
import org.jooq.Record3;
import org.jooq.RecordMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.mapbox.model.db.tables.Polygon.POLYGON;
import static com.example.mapbox.utils.PostGisUtils.*;

@Repository
@RequiredArgsConstructor
public class PolygonRepositoryImpl implements PolygonRepository {
    private final DSLContext dslContext;

    @Override
    public int save(List<List<List<Double>>> coordinates) {
        return dslContext
                .insertInto(POLYGON)
                .set(POLYGON.SQUARE, getSquareField(coordinates))
                .set(POLYGON.GEOMETRY, getPolygonField(coordinates))
                .execute();
    }

    @Override
    public List<Polygon> findAll() {
        return dslContext
                .select(POLYGON.ID, POLYGON.SQUARE, getGeometryAsText(POLYGON.GEOMETRY))
                .from(POLYGON)
                .fetch(returnPolygonFromRecordMapper());
    }

    @Override
    public int saveAll(List<Polygon> polygons) {
        return dslContext.batch(getInsertReturningStep(polygons)).execute().length;
    }

    @Override
    public int deleteAll() {
        return dslContext
                .deleteFrom(POLYGON)
                .execute();
    }

    @Override
    public boolean isPolygonsExists() {
        return dslContext.fetchExists(POLYGON);
    }

    private List<InsertReturningStep<PolygonRecord>> getInsertReturningStep(List<Polygon> polygons) {
        return polygons.stream()
                .map(polygon -> dslContext
                        .insertInto(POLYGON, POLYGON.SQUARE, POLYGON.GEOMETRY)
                        .values(polygon.getSquare(), getPolygonAsTextFromCoordinates(polygon.getCoordinates()))
                ).collect(Collectors.toUnmodifiableList());
    }

    private RecordMapper<Record3<Long, Double, String>, Polygon> returnPolygonFromRecordMapper() {
        return record ->
                new Polygon(
                        record.get(POLYGON.ID),
                        record.get(POLYGON.SQUARE),
                        getPolygonCoordinatesFromString(record.get(getGeometryAsText(POLYGON.GEOMETRY)))
                );
    }
}
