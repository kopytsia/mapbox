package com.example.mapbox.repository.impl;

import com.example.mapbox.model.Segment;
import com.example.mapbox.model.db.tables.Polygon;
import com.example.mapbox.model.db.tables.records.SegmentRecord;
import com.example.mapbox.repository.SegmentRepository;
import lombok.RequiredArgsConstructor;
import org.jooq.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.mapbox.model.db.Tables.POLYGON;
import static com.example.mapbox.model.db.Tables.SEGMENT;
import static com.example.mapbox.utils.PostGisUtils.*;
import static org.jooq.impl.DSL.when;

@Repository
@RequiredArgsConstructor
public class SegmentRepositoryImpl implements SegmentRepository {
    private final DSLContext dslContext;

    @Override
    public int save(List<List<Double>> coordinates) {
        return dslContext
                .insertInto(SEGMENT)
                .set(SEGMENT.LENGTH, getLengthField(coordinates))
                .set(SEGMENT.GEOMETRY, getLineStringField(coordinates))
                .execute();
    }

    @Override
    public List<Segment> findAll() {
        CommonTableExpression<Record1<Object>> table = getPolygonUnionTable(Polygon.POLYGON);
        return dslContext
                .with(getPolygonUnionTable(POLYGON))
                .select(
                        SEGMENT.ID,
                        SEGMENT.LENGTH,
                        getGeometryAsText(SEGMENT.GEOMETRY),
                        when(getWithinFieldFromGeometry(SEGMENT, table.field("polygon")).isTrue(), SEGMENT_WITHIN_POLYGONS_COLOR).otherwise(SEGMENT_DEFAULT_COLOR).as("color")
                )
                .from(SEGMENT, table)
                .fetch(returnSegmentFromRecordMapper());
    }

    @Override
    public int saveAll(List<Segment> segments) {
        return dslContext.batch(getInsertReturningStep(segments)).execute().length;
    }

    @Override
    public int deleteAll() {
        return dslContext
                .deleteFrom(SEGMENT)
                .execute();
    }

    @Override
    public boolean isSegmentsExists() {
        return dslContext.fetchExists(SEGMENT);
    }

    private List<InsertReturningStep<SegmentRecord>> getInsertReturningStep(List<Segment> segments) {
        return segments.stream()
                .map(segment -> dslContext
                        .insertInto(SEGMENT, SEGMENT.LENGTH, SEGMENT.GEOMETRY)
                        .values(segment.getLength(), getSegmentAsTextFromCoordinates(segment.getCoordinates()))
                ).collect(Collectors.toUnmodifiableList());
    }

    private RecordMapper<Record4<Long, Double, String, String>, Segment> returnSegmentFromRecordMapper() {
        return record ->
                new Segment(
                        record.get(SEGMENT.ID),
                        record.get(SEGMENT.LENGTH),
                        getSegmentCoordinatesFromString(record.get(getGeometryAsText(SEGMENT.GEOMETRY))),
                        record.get(getSegmentColorField())
                );
    }
}
