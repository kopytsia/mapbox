package com.example.mapbox.repository.impl;

import com.example.mapbox.model.BackupGeometryType;
import com.example.mapbox.model.BackupInfo;
import com.example.mapbox.repository.BackupInfoRepository;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import static com.example.mapbox.model.db.tables.BackupInfo.BACKUP_INFO;

@Repository
@RequiredArgsConstructor
public class BackupInfoRepositoryImpl implements BackupInfoRepository {
    private final DSLContext dslContext;
    
    @Override
    public int create(String filename, BackupGeometryType geometryType) {
        return dslContext
                .insertInto(BACKUP_INFO, BACKUP_INFO.CREATION_DATE, BACKUP_INFO.FILENAME, BACKUP_INFO.GEOMETRY_TYPE)
                .values(Timestamp.valueOf(LocalDateTime.now()), filename, geometryType.name())
                .execute();
    }

    @Override
    public List<BackupInfo> findAll() {
        return dslContext
                .selectFrom(BACKUP_INFO)
                .fetchInto(BackupInfo.class);
    }
}
