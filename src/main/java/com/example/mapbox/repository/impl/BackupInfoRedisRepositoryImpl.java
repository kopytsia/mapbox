package com.example.mapbox.repository.impl;

import com.example.mapbox.model.BackupGeometryType;
import com.example.mapbox.model.BackupInfo;
import com.example.mapbox.repository.BackupInfoRedisRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.*;

@Repository
@Slf4j
public class BackupInfoRedisRepositoryImpl implements BackupInfoRedisRepository {
    private final RedisTemplate<String, BackupInfo> redisTemplate;
    private final ValueOperations<String, BackupInfo> valueOperations;


    public BackupInfoRedisRepositoryImpl(RedisTemplate<String, BackupInfo> redisTemplate) {
        this.redisTemplate = redisTemplate;
        this.valueOperations = redisTemplate.opsForValue();
    }

    @Override
    public void create(BackupInfo backupInfo) {
        String key = backupInfo.getType().toString() + "_" + backupInfo.getFilename();
        valueOperations.set(key, backupInfo);
        log.info("Backup info with key: " + key + " was created!");
    }

    @Override
    public List<BackupInfo> findAll() {
        return Optional.ofNullable(redisTemplate.keys("*"))
                .map(valueOperations::multiGet).orElse(Collections.emptyList());
    }

    @Override
    public List<BackupInfo> findAllByDate(LocalDate date) {
        return Optional.ofNullable(redisTemplate.keys("*" + date + "*"))
                .map(valueOperations::multiGet).orElse(Collections.emptyList());
    }

    @Override
    public Optional<BackupInfo> getLastByType(BackupGeometryType type) {
        return Optional.ofNullable(redisTemplate.keys("*" + type.name() + "*"))
                .map(valueOperations::multiGet)
                .flatMap(backups -> backups.stream()
                        .max(Comparator.comparing(BackupInfo::getCreationDate))
                );
    }
}
