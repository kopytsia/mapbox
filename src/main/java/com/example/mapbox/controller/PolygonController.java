package com.example.mapbox.controller;

import com.example.mapbox.service.PolygonService;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/polygon")
@RequiredArgsConstructor
public class PolygonController {
    private final PolygonService polygonService;
    private final MeterRegistry meterRegistry;

    @PostMapping("save")
    public ResponseEntity<Void> save(@RequestBody List<List<List<Double>>> coordinates) {
        meterRegistry.counter("polygon_save").increment();
        if (polygonService.save(coordinates)) {
            meterRegistry.counter("polygon_success_save").increment();
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        meterRegistry.counter("polygon_failure_save").increment();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
}
