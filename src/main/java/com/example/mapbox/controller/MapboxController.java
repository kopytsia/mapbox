package com.example.mapbox.controller;

import com.example.mapbox.model.BackupInfo;
import com.example.mapbox.model.MapboxFeatureCollection;
import com.example.mapbox.model.MapboxToken;
import com.example.mapbox.service.MapboxService;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("api/mapbox")
@RequiredArgsConstructor
@Slf4j
public class MapboxController {
    private final MapboxService mapboxService;
    private final MeterRegistry meterRegistry;

    @GetMapping("token")
    public ResponseEntity<MapboxToken> getToken() {
        return ResponseEntity.ok(mapboxService.getToken());
    }

    @GetMapping
    public ResponseEntity<MapboxFeatureCollection> getFeatureCollection() {
        return ResponseEntity.ok(mapboxService.getFeatureCollection());
    }

    @PostMapping("backup")
    @Timed("backup_time")
    public ResponseEntity<Void> backup() {
        meterRegistry.counter("backup").increment();
        try {
            if (mapboxService.backupGeometries()) {
                meterRegistry.counter("backup_success").increment();
                return ResponseEntity.status(HttpStatus.CREATED).build();
            }
            meterRegistry.counter("backup_failure").increment();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (IOException e) {
            meterRegistry.counter("backup_failure").increment();
            log.error("Can`t make new backup!", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("restore")
    @Timed("restore_time")
    public ResponseEntity<Void> restore(@RequestBody BackupInfo backupInfo) {
        meterRegistry.counter("restore").increment();
        try {
            if (mapboxService.restoreGeometries(backupInfo)) {
                meterRegistry.counter("restore_success").increment();
                return ResponseEntity.status(HttpStatus.CREATED).build();
            }
            meterRegistry.counter("restore_failure").increment();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (IOException e) {
            meterRegistry.counter("restore_failure").increment();
            log.error("Can`t restore data!", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
