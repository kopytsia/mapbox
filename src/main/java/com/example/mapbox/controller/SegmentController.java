package com.example.mapbox.controller;

import com.example.mapbox.service.SegmentService;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/segment")
@RequiredArgsConstructor
public class SegmentController {
    private final SegmentService segmentService;
    private final MeterRegistry meterRegistry;

    @PostMapping("save")
    public ResponseEntity<Void> save(@RequestBody List<List<Double>> coordinates) {
        meterRegistry.counter("segment_save").increment();
        if (segmentService.save(coordinates)) {
            meterRegistry.counter("segment_success_save").increment();
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        meterRegistry.counter("segment_failure_save").increment();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
}
