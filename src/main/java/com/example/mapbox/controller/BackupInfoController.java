package com.example.mapbox.controller;

import com.example.mapbox.model.BackupInfo;
import com.example.mapbox.service.BackupInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE;

@RestController
@RequestMapping("api/backup/info")
@RequiredArgsConstructor
public class BackupInfoController {
    private final BackupInfoService backupInfoService;

    @GetMapping
    public ResponseEntity<List<BackupInfo>> getAll() {
        return ResponseEntity.ok(backupInfoService.getAll());
    }

    @GetMapping("{date}")
    public ResponseEntity<List<BackupInfo>> getAllByDate(@PathVariable @DateTimeFormat(iso = DATE) LocalDate date) {
        return ResponseEntity.ok(backupInfoService.getAllByDate(date));
    }
}
