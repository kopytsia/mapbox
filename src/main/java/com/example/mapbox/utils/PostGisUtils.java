package com.example.mapbox.utils;

import com.example.mapbox.model.db.tables.Polygon;
import com.example.mapbox.model.db.tables.Segment;
import com.example.mapbox.model.db.tables.records.PolygonRecord;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.jooq.CommonTableExpression;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.TableField;
import org.jooq.impl.DSL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.*;

@Slf4j
public class PostGisUtils {
    private static final String GET_LENGTH_FUNCTION = "ST_Length('%s'::geography)";
    private static final String GET_SQUARE_FUNCTION = "ST_AREA('%s'::geography)";
    private static final String GET_SEGMENT_FUNCTION = "LINESTRING(%s)";
    private static final String GET_POLYGON_FUNCTION = "POLYGON((%s))";
    public static final String SEGMENT_WITHIN_POLYGONS_COLOR = "#F7455D";
    public static final String SEGMENT_DEFAULT_COLOR = "#33C9EB";
    private static final String SPACE = " ";
    private static final String COMMA = ",";

    public static String getLengthFunction(String segmentAsText) {
        return String.format(GET_LENGTH_FUNCTION, segmentAsText);
    }

    public static String getSquareFunction(String polygonAsText) {
        return String.format(GET_SQUARE_FUNCTION, polygonAsText);
    }

    public static Field<Double> getSquareField(List<List<List<Double>>> coordinates) {
        return field(getSquareFunction(getPolygonAsTextFromCoordinates(coordinates)), Double.class);
    }

    public static Field<String> getPolygonField(List<List<List<Double>>> coordinates) {
        return field("({0}::GEOMETRY)", String.class, getPolygonAsTextFromCoordinates(coordinates));
    }

    public static Field<String> getGeometryAsText(TableField<?, Object> geometry) {
        return DSL.field("regexp_replace(st_astext({0}), '[A-Z()]', '', 'g')", String.class, geometry);
    }

    public static String createLineStringFunction(String geometryAsText) {
        return String.format(GET_SEGMENT_FUNCTION, geometryAsText);
    }

    public static Field<Double> getLengthField(List<List<Double>> coordinates) {
        return field(getLengthFunction(getSegmentAsTextFromCoordinates(coordinates)), Double.class);
    }

    public static Field<String> getLineStringField(List<List<Double>> coordinates) {
        return field("({0}::GEOMETRY)", String.class, getSegmentAsTextFromCoordinates(coordinates));
    }

    public static Field<String> getSegmentColorField() {
        return field("color", String.class);
    }

    public static CommonTableExpression<Record1<Object>> getPolygonUnionTable(Polygon polygon) {
        return name("unions").fields("polygon").as(select(getUnionFieldFromGeometry(polygon.GEOMETRY)).from(polygon));
    }

    public static Field<Boolean> getWithinFieldFromGeometry(Segment segment, Field<?> polygonField) {
        return field("st_within({0}, {1})", Boolean.class, segment.GEOMETRY, polygonField);
    }

    public static Field<Object> getUnionFieldFromGeometry(TableField<PolygonRecord, Object> geometry) {
        return field("st_union({0})", geometry);
    }
    public static List<List<Double>> getSegmentCoordinatesFromString(String segmentAsText) {
        if (Strings.isNotBlank(segmentAsText)) {
            String[] points = segmentAsText.split(COMMA);
            List<List<Double>> result = new ArrayList<>();
            for (String point : points) {
                String[] coordinates = point.split(SPACE);
                result.add(List.of(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1])));
            }
            return result;
        } else {
            log.error("Segment as text not valid or empty! segmentAsTest = " + segmentAsText);
        }
        return Collections.emptyList();
    }

    public static List<List<List<Double>>> getPolygonCoordinatesFromString(String polygonAsText) {
        if (Strings.isNotBlank(polygonAsText)) {
            String[] coordinatesArray = polygonAsText.split(COMMA);
            return List.of(Arrays.stream(coordinatesArray)
                    .map(coordinate -> {
                        String[] coordinateArray = coordinate.split(SPACE);
                        return List.of(Double.parseDouble(coordinateArray[0]), Double.parseDouble(coordinateArray[1]));
                    })
                    .collect(Collectors.toUnmodifiableList()));
        } else {
            log.error("Polygon as text not valid or empty! polygonAsTest = " + polygonAsText);
        }
        return Collections.emptyList();
    }

    public static String getPolygonAsTextFromCoordinates(List<List<List<Double>>> coordinates) {
        return String.format(GET_POLYGON_FUNCTION, returnCoordinatesAsString(coordinates.get(0)));
    }

    public static String getSegmentAsTextFromCoordinates(List<List<Double>> coordinates) {
        return String.format(GET_SEGMENT_FUNCTION, returnCoordinatesAsString(coordinates));
    }

    private static String returnCoordinatesAsString(List<List<Double>> coordinates) {
        return coordinates
                .stream()
                .map(coordinate -> coordinate.get(0) + " " + coordinate.get(1))
                .collect(Collectors.joining(", "));
    }
}
