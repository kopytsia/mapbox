package com.example.mapbox.service.impl;

import com.example.mapbox.client.MapboxClient;
import com.example.mapbox.config.MapboxConfig;
import com.example.mapbox.converter.MapboxConverter;
import com.example.mapbox.converter.PolygonConverter;
import com.example.mapbox.converter.SegmentConverter;
import com.example.mapbox.model.*;
import com.example.mapbox.service.MapboxService;
import com.example.mapbox.service.PolygonService;
import com.example.mapbox.service.SegmentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
@RequiredArgsConstructor
public class MapboxServiceImpl implements MapboxService {
    private static final int BEGIN_ID_INDEX = 33;
    private final SegmentService segmentService;
    private final PolygonService polygonService;
    private final PolygonConverter polygonConverter;
    private final SegmentConverter segmentConverter;
    private final MapboxConverter mapboxConverter;
    private final MapboxClient mapboxClient;
    private final MapboxConfig mapboxConfig;

    @Override
    public MapboxToken getToken() {
        return new MapboxToken(mapboxConfig.getMapboxToken());
    }

    @Override
    public MapboxFeatureCollection getFeatureCollection() {
        Stream<MapboxFeature> segments = getStreamOfMapboxFeatureFromSegments(segmentService.getAll());
        Stream<MapboxFeature> polygons = getStreamOfMapboxFeatureFromPolygons(polygonService.getAll());
        List<MapboxFeature> features = Stream.concat(segments,polygons).collect(Collectors.toUnmodifiableList());
        return new MapboxFeatureCollection(features);
    }

    @Override
    public boolean backupGeometries() throws IOException {
        String filename = LocalDateTime.now().toString();
        boolean polygonsBackup = polygonService.backup(filename);
        boolean segmentsBackup = segmentService.backup(filename);
        return polygonsBackup && segmentsBackup;
    }

    @Override
    public boolean restoreGeometries(BackupInfo backupInfo) throws IOException {
        String filename = LocalDateTime.now().toString();
        if (BackupGeometryType.POLYGON.equals(backupInfo.getType())) {
            if (polygonService.isPolygonsExists()) {
                if (polygonService.backup(filename)) {
                    polygonService.deleteAll();
                } else {
                    return false;
                }
            }
            return polygonService.restore(backupInfo.getFilename());
        }
        if (BackupGeometryType.SEGMENT.equals(backupInfo.getType())) {
            if (segmentService.isSegmentsExists()) {
                if (segmentService.backup(filename)) {
                    segmentService.deleteAll();
                } else {
                    return false;
                }
            }
            return segmentService.restore(backupInfo.getFilename());
        }
        return false;
    }

    @Override
    public File getLastBackupsAsLineDelimitedGeoJsonFile() throws IOException {
        Stream<MapboxFeature> segments = getStreamOfMapboxFeatureFromSegments(segmentService.getAllLastBackup());
        Stream<MapboxFeature> polygons = getStreamOfMapboxFeatureFromPolygons(polygonService.getAllLastBackup());
        List<MapboxFeature> features = Stream.concat(segments, polygons).collect(Collectors.toUnmodifiableList());
        return mapboxConverter.toLineDelimitedGeoJsonFile(features);
    }

    @Override
    public Tileset createTileset(String sourceId) {
        String id = mapboxConfig.getTilesetId();
        String source = String.format(mapboxConfig.getSourceTemplate(), mapboxConfig.getMapboxUsername(), sourceId);
        TilesetSource tilesetSource = new TilesetSource(source);
        Map<String, TilesetSource> layers = Map.of(id, tilesetSource);
        Recipe recipe = new Recipe(layers);
        return new Tileset(recipe, id);
    }

    @Override
    public void createSource(String id, File file) {
        HttpStatus status = mapboxClient.createTilesetSource(id, file);
        if (status == HttpStatus.OK) {
            log.info("Source was created, id: " + id);
        } else {
            log.error("Source was not created, id: " + id, status.getReasonPhrase());
        }
    }

    @Override
    public void updateTileset(Tileset tileset) {
        HttpStatus status = mapboxClient.updateTileset(tileset);
        if (status == HttpStatus.NO_CONTENT) {
            log.info("Tileset was updated, id: " + tileset.getName());
        } else {
            log.error("Tileset was not updated, id: " + tileset.getName(), status.getReasonPhrase());
        }
    }

    @Override
    public void deleteOldSources(String newSourceId) {
        ResponseEntity<TilesetResponseBody[]> responseEntity = mapboxClient.getTilesets();
        TilesetResponseBody[] body = responseEntity.getBody();
        List<String> oldIds = fetchListOfOldIds(newSourceId, body);
        for (String id : oldIds) {
            mapboxClient.deleteSource(id);
        }
    }

    @Override
    public void publishTileset() {
        String id = mapboxConfig.getTilesetId();
        HttpStatus status = mapboxClient.publishTileset(id);
        if (status == HttpStatus.OK) {
            log.info("Tileset publish process was started, id: " + id);
        } else {
            log.error("Can`t start publish process, id: " + id, status.getReasonPhrase());
        }
    }

    private List<String> fetchListOfOldIds(String newSourceId, TilesetResponseBody[] body) {
        return Arrays.stream(body)
                .filter(response -> !response.getId().contains(newSourceId))
                .map(response -> response.getId().substring(BEGIN_ID_INDEX))
                .collect(Collectors.toUnmodifiableList());
    }

    private Stream<MapboxFeature> getStreamOfMapboxFeatureFromSegments(List<Segment> segments) {
        return segments.stream()
                .map(segmentConverter::toMapboxFeature);
    }

    private Stream<MapboxFeature> getStreamOfMapboxFeatureFromPolygons(List<Polygon> polygons) {
        return polygons.stream()
                .map(polygonConverter::toMapboxFeature);
    }
}
