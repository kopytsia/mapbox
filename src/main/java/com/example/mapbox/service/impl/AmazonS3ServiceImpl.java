package com.example.mapbox.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.example.mapbox.service.AmazonS3Service;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AmazonS3ServiceImpl implements AmazonS3Service {
    private static final String S3_BUCKET_NAME = "mapbox-bucket";
    private final AmazonS3 amazonS3;

    @Override
    public void upload(String filename, File file) {
        amazonS3.putObject(S3_BUCKET_NAME, filename, file);
    }

    @Override
    public Optional<File> download(String filename) throws IOException {
        File tempFile = File.createTempFile(filename, ".json");
        GetObjectRequest request = new GetObjectRequest(S3_BUCKET_NAME, filename);
        amazonS3.getObject(request, tempFile);
        return Optional.of(tempFile);
    }
}
