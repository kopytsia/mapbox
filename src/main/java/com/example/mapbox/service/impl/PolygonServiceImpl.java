package com.example.mapbox.service.impl;

import com.example.mapbox.converter.PolygonConverter;
import com.example.mapbox.model.BackupInfo;
import com.example.mapbox.model.Polygon;
import com.example.mapbox.repository.PolygonRepository;
import com.example.mapbox.service.AmazonS3Service;
import com.example.mapbox.service.BackupInfoService;
import com.example.mapbox.service.PolygonService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.example.mapbox.model.BackupGeometryType.POLYGON;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;

@Service
@RequiredArgsConstructor
@Slf4j
public class PolygonServiceImpl implements PolygonService {
    private final PolygonRepository polygonRepository;
    private final PolygonConverter polygonConverter;
    private final AmazonS3Service amazonS3Service;
    private final BackupInfoService backupInfoService;
    private static final String SUFFIX = "_polygons.json";

    @Override
    public boolean save(List<List<List<Double>>> coordinates) {
        int affectedRows = polygonRepository.save(coordinates);
        return affectedRows > INTEGER_ZERO;
    }

    @Override
    public List<Polygon> getAll() {
        return polygonRepository.findAll();
    }

    @Override
    public boolean backup(String filename) throws IOException {
        List<Polygon> polygons = polygonRepository.findAll();
        if (!polygons.isEmpty()) {
            File tempFile = polygonConverter.toFile(filename, SUFFIX, polygons);
            amazonS3Service.upload(filename + SUFFIX, tempFile);
            return backupInfoService.create(filename + SUFFIX, POLYGON);
        }
        return false;
    }

    @Override
    public boolean restore(String filename) throws IOException {
        Optional<File> file = amazonS3Service.download(filename);
        if (file.isPresent()) {
            return saveAllFromFile(file.get());
        }
        return false;
    }

    @Override
    public boolean deleteAll() {
        return polygonRepository.deleteAll() > INTEGER_ZERO;
    }

    @Override
    public boolean isPolygonsExists() {
        return polygonRepository.isPolygonsExists();
    }

    @Override
    public List<Polygon> getAllLastBackup() throws IOException {
        Optional<BackupInfo> backupInfo = backupInfoService.getLastByType(POLYGON);
        if (backupInfo.isPresent()) {
            Optional<File> file = amazonS3Service.download(backupInfo.get().getFilename());
            if (file.isPresent()) {
                return polygonConverter.fromFile(file.get());
            }
        }
        return Collections.emptyList();
    }

    private boolean saveAllFromFile(File file) throws IOException {
        List<Polygon> polygons = polygonConverter.fromFile(file);
        return polygonRepository.saveAll(polygons) > INTEGER_ZERO;
    }
}
