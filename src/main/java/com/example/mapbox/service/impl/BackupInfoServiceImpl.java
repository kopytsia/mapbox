package com.example.mapbox.service.impl;

import com.example.mapbox.model.BackupGeometryType;
import com.example.mapbox.model.BackupInfo;
import com.example.mapbox.repository.BackupInfoRedisRepository;
import com.example.mapbox.service.BackupInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class BackupInfoServiceImpl implements BackupInfoService {
    private final BackupInfoRedisRepository backupInfoRepository;

    @Override
    public boolean create(String filename, BackupGeometryType geometryType) {
        backupInfoRepository.create(new BackupInfo(LocalDateTime.now(), filename, geometryType));
        return true;
    }

    @Override
    public List<BackupInfo> getAll() {
        return backupInfoRepository.findAll();
    }

    @Override
    public List<BackupInfo> getAllByDate(LocalDate date) {
        return backupInfoRepository.findAllByDate(date);
    }

    @Override
    public Optional<BackupInfo> getLastByType(BackupGeometryType type) {
        return backupInfoRepository.getLastByType(type);
    }

}
