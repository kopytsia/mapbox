package com.example.mapbox.service.impl;

import com.example.mapbox.converter.SegmentConverter;
import com.example.mapbox.model.BackupInfo;
import com.example.mapbox.model.Segment;
import com.example.mapbox.repository.SegmentRepository;
import com.example.mapbox.service.AmazonS3Service;
import com.example.mapbox.service.BackupInfoService;
import com.example.mapbox.service.SegmentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.example.mapbox.model.BackupGeometryType.SEGMENT;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;

@Service
@Slf4j
@RequiredArgsConstructor
public class SegmentServiceImpl implements SegmentService {
    private static final String SUFFIX = "_segments.json";
    private static final int SEGMENT_LIST_SIZE = 2;
    private final SegmentRepository segmentRepository;
    private final SegmentConverter segmentConverter;
    private final AmazonS3Service amazonS3Service;
    private final BackupInfoService backupInfoService;

    @Override
    public boolean save(List<List<Double>> coordinates) {
        if (coordinates.size() == SEGMENT_LIST_SIZE) {
            int affectedRows = segmentRepository.save(coordinates);
            return affectedRows > INTEGER_ZERO;
        }
        return false;
    }

    @Override
    public List<Segment> getAll() {
        return segmentRepository.findAll();
    }


    @Override
    public boolean backup(String filename) throws IOException {
        List<Segment> segments = segmentRepository.findAll();
        if (!segments.isEmpty()) {
            File tempFile = segmentConverter.toFile(filename, SUFFIX, segments);
            amazonS3Service.upload(filename + SUFFIX, tempFile);
            return backupInfoService.create(filename + SUFFIX, SEGMENT);
        }
        return false;
    }

    @Override
    public boolean restore(String filename) throws IOException {
        Optional<File> file = amazonS3Service.download(filename);
        if (file.isPresent()) {
            return saveAllFromFile(file.get());
        }
        return false;
    }

    @Override
    public boolean deleteAll() {
        return segmentRepository.deleteAll() > INTEGER_ZERO;
    }

    @Override
    public boolean isSegmentsExists() {
        return segmentRepository.isSegmentsExists();
    }

    @Override
    public List<Segment> getAllLastBackup() throws IOException {
        Optional<BackupInfo> backupInfo = backupInfoService.getLastByType(SEGMENT);
        if (backupInfo.isPresent()) {
            Optional<File> file = amazonS3Service.download(backupInfo.get().getFilename());
            if (file.isPresent()) {
                return segmentConverter.fromFile(file.get());
            }
        }
        return Collections.emptyList();
    }

    private boolean saveAllFromFile(File file) throws IOException {
        List<Segment> segments = segmentConverter.fromFile(file);
        return segmentRepository.saveAll(segments) > INTEGER_ZERO;
    }
}
