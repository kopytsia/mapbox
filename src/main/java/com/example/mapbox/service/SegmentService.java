package com.example.mapbox.service;

import com.example.mapbox.model.Segment;

import java.io.IOException;
import java.util.List;

public interface SegmentService {
    boolean save(List<List<Double>> coordinates);

    List<Segment> getAll();

    boolean backup(String filename) throws IOException;

    boolean restore(String filename) throws IOException;

    boolean deleteAll();

    boolean isSegmentsExists();

    List<Segment> getAllLastBackup() throws IOException;
}
