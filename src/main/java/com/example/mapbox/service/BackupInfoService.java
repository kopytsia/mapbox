package com.example.mapbox.service;

import com.example.mapbox.model.BackupGeometryType;
import com.example.mapbox.model.BackupInfo;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface BackupInfoService {
    boolean create(String filename, BackupGeometryType geometryType);

    List<BackupInfo> getAll();

    List<BackupInfo> getAllByDate(LocalDate date);

    Optional<BackupInfo> getLastByType(BackupGeometryType type);
}
