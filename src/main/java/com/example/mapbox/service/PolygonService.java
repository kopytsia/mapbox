package com.example.mapbox.service;

import com.example.mapbox.model.Polygon;

import java.io.IOException;
import java.util.List;

public interface PolygonService {
    boolean save(List<List<List<Double>>> coordinates);

    List<Polygon> getAll();

    boolean backup(String filename) throws IOException;

    boolean restore(String filename) throws IOException;

    boolean deleteAll();

    boolean isPolygonsExists();

    List<Polygon> getAllLastBackup() throws IOException;
}
