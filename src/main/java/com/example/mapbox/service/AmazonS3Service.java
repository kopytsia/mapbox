package com.example.mapbox.service;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

public interface AmazonS3Service {
    void upload(String filename, File file);

    Optional<File> download(String filename) throws IOException;
}
