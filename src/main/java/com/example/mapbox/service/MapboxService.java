package com.example.mapbox.service;

import com.example.mapbox.model.BackupInfo;
import com.example.mapbox.model.MapboxFeatureCollection;
import com.example.mapbox.model.MapboxToken;
import com.example.mapbox.model.Tileset;

import java.io.File;
import java.io.IOException;

public interface MapboxService {
    MapboxToken getToken();

    MapboxFeatureCollection getFeatureCollection();

    boolean backupGeometries() throws IOException;

    boolean restoreGeometries(BackupInfo backupInfo) throws IOException;

    File getLastBackupsAsLineDelimitedGeoJsonFile() throws IOException;

    Tileset createTileset(String sourceId);

    void createSource(String id, File file);

    void updateTileset(Tileset tileset);

    void deleteOldSources(String newSourceId);

    void publishTileset();
}
