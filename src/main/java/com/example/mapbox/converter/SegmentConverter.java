package com.example.mapbox.converter;

import com.example.mapbox.model.MapboxFeature;
import com.example.mapbox.model.Segment;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface SegmentConverter {
    MapboxFeature toMapboxFeature(Segment segment);

    File toFile(String filename, String suffix, List<Segment> segments) throws IOException;

    List<Segment> fromFile(File file) throws IOException;
}
