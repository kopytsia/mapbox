package com.example.mapbox.converter;

import com.example.mapbox.model.MapboxFeature;
import com.example.mapbox.model.Polygon;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface PolygonConverter {
    MapboxFeature toMapboxFeature(Polygon polygon);

    List<Polygon> fromFile(File file) throws IOException;

    File toFile(String filename, String suffix, List<Polygon> polygons) throws IOException;
}