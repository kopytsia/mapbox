package com.example.mapbox.converter;

import com.example.mapbox.model.MapboxFeature;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface MapboxConverter {
    File toLineDelimitedGeoJsonFile(List<MapboxFeature> mapboxFeatures) throws IOException;
}
