package com.example.mapbox.converter.impl;

import com.example.mapbox.converter.PolygonConverter;
import com.example.mapbox.model.MapboxFeature;
import com.example.mapbox.model.MapboxGeometry;
import com.example.mapbox.model.Polygon;
import com.example.mapbox.model.PolygonMapboxGeometry;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class PolygonConverterImpl implements PolygonConverter {
    private final ObjectMapper objectMapper;

    @Override
    public MapboxFeature toMapboxFeature(Polygon polygon) {
        MapboxGeometry mapboxGeometry = new PolygonMapboxGeometry(polygon.getCoordinates());
        Map<String, Object> properties = Map.of(
                "id", polygon.getId(),
                "square", polygon.getSquare()
        );
        return new MapboxFeature(mapboxGeometry, properties);
    }

    @Override
    public File toFile(String filename, String suffix, List<Polygon> polygons) throws IOException {
        File tempFile = File.createTempFile(filename, suffix);
        objectMapper.writeValue(tempFile, polygons);
        return tempFile;
    }

    @Override
    public List<Polygon> fromFile(File file) throws IOException {
        return List.of(objectMapper.readValue(file, Polygon[].class));
    }
}
