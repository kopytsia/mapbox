package com.example.mapbox.converter.impl;

import com.example.mapbox.converter.SegmentConverter;
import com.example.mapbox.model.MapboxFeature;
import com.example.mapbox.model.MapboxGeometry;
import com.example.mapbox.model.Segment;
import com.example.mapbox.model.SegmentMapboxGeometry;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class SegmentConverterImpl implements SegmentConverter {
    private final ObjectMapper objectMapper;

    @Override
    public MapboxFeature toMapboxFeature(Segment segment) {
        MapboxGeometry mapboxGeometry = new SegmentMapboxGeometry(segment.getCoordinates());
        Map<String, Object> properties = Map.of(
                "id", segment.getId(),
                "length", segment.getLength(),
                "color", segment.getColor()
        );
        return new MapboxFeature(mapboxGeometry, properties);
    }

    @Override
    public File toFile(String filename, String suffix, List<Segment> segments) throws IOException {
        File tempFile = File.createTempFile(filename, suffix);
        objectMapper.writeValue(tempFile, segments);
        return tempFile;
    }

    @Override
    public List<Segment> fromFile(File file) throws IOException {
        return List.of(objectMapper.readValue(file, Segment[].class));
    }
}
