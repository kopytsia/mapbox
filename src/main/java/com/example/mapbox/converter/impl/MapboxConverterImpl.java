package com.example.mapbox.converter.impl;

import com.example.mapbox.converter.MapboxConverter;
import com.example.mapbox.model.MapboxFeature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class MapboxConverterImpl implements MapboxConverter {
    private final ObjectMapper objectMapper;

    @Override
    public File toLineDelimitedGeoJsonFile(List<MapboxFeature> mapboxFeatures) throws IOException{
        Path path = Files.createTempFile(UUID.randomUUID().toString(), ".geojson.ld");
        List<String> features = new ArrayList<>();
        for (MapboxFeature feature : mapboxFeatures) {
            try {
                features.add(objectMapper.writeValueAsString(feature));
            } catch (JsonProcessingException e) {
                log.error("Some problem with feature!", e);
            }
        }
        Files.write(path, features);
        return path.toFile();
    }
}
