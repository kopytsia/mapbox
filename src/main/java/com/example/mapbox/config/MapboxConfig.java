package com.example.mapbox.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "mapbox")
@Configuration
@Getter
@Setter
public class MapboxConfig {
    private String sourceReplaceUrl;
    private String sourceCreateUrl;
    private String sourceListUrl;
    private String sourceDeleteUrl;
    private String tilesetCreateUrl;
    private String tilesetUpdateUrl;
    private String tilesetPublishUrl;
    private String tilesetId;
    private String mapboxSkToken;
    private String mapboxUsername;
    private String mapboxToken;
    private String sourceTemplate;
}
