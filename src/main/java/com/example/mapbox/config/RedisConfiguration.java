package com.example.mapbox.config;

import com.example.mapbox.model.BackupInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
@EnableRedisRepositories
public class RedisConfiguration {
    private final String redisHost;
    private final Integer redisPort;

    public RedisConfiguration(
            @Value("#{environment.REDIS_HOST}") String redisHost,
            @Value("#{environment.REDIS_PORT}") Integer redisPort) {
        this.redisHost = redisHost;
        this.redisPort = redisPort;
    }

    @Bean
    public JedisConnectionFactory jedisConnectionFactory() {
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
        redisStandaloneConfiguration.setHostName(redisHost);
        redisStandaloneConfiguration.setPort(redisPort);
        return new JedisConnectionFactory(redisStandaloneConfiguration);
    }

    @Bean
    public RedisTemplate<String, BackupInfo> redisTemplate(Jackson2JsonRedisSerializer<BackupInfo> serializer) {
        RedisTemplate<String, BackupInfo> template = new RedisTemplate<>();
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(serializer);
        template.setConnectionFactory(jedisConnectionFactory());
        return template;
    }

    @Bean
    public Jackson2JsonRedisSerializer<BackupInfo> redisSerializer(ObjectMapper objectMapper) {
        Jackson2JsonRedisSerializer<BackupInfo> serializer = new Jackson2JsonRedisSerializer<>(BackupInfo.class);
        serializer.setObjectMapper(objectMapper);
        return serializer;
    }
}
