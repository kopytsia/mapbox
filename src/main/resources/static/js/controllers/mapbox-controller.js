angular.module('app')
    .controller(
        'mapboxController', ['mapboxService', 'segmentService', 'polygonService', 'backupInfoService', '$scope',
            function (mapboxService, segmentService, polygonService, backupInfoService, $scope
            ) {
                const EXISTS_GEOMETRIES_SOURCE_NAME = "existsGeometries";
                const EXISTS_GEOMETRIES_LINES_LAYER_NAME = "existsGeometriesLines";
                const EXISTS_GEOMETRIES_FILL_LAYER_NAME = "existsGeometriesFill";
                const EXISTS_GEOMETRIES_MARKER_LAYER_NAME = "existsGeometriesMarker";
                const NEW_GEOMETRIES_SOURCE_NAME = "newGeometries";
                const NEW_GEOMETRIES_POINTS_LAYER_NAME = "newGeometriesPoints";
                const NEW_GEOMETRIES_LINES_LAYER_NAME = "newGeometriesLines";
                const NEW_GEOMETRIES_FILL_LAYER_NAME = "newGeometriesFill";
                const START_POINT_INDEX = 0;
                const END_POINT_INDEX = 1;
                const SEGMENT_COUNT = 2;
                const CIRCLE_RADIUS = 5;
                const CIRCLE_COLOR = '#000';
                const LINE_COLOR = '#000';
                const LINE_WIDTH = 2.5;
                const FILL_COLOR = '#088';
                const FILL_OPACITY = 0.3;
                const POLYGON_LAYER_TYPE_NAME = "fill";
                const SEGMENT_LAYER_TYPE_NAME = "line"
                const STYLE_ID = "mapbox://styles/kopytsia/ckhulcm0o0e3m19nxmpffn0qs";


                let marker;
                let map;
                let existsFeatureCollection = {
                    'type': 'FeatureCollection',
                    'features': []
                };
                let featureCollection = {
                    'type': 'FeatureCollection',
                    'features': []
                };
                $scope.info = [];
                $scope.drawingModes = {
                    SEGMENT: 'segment',
                    POLYGON: 'polygon'
                };
                $scope.backupInfos = [];
                $scope.backupDate = '';
                $scope.currentDrawingMode = $scope.drawingModes.SEGMENT;
                $scope.currentBackupInfo = {};

                mapboxService.getToken().then(response => {
                    map = mapboxService.createMap(response.data.token);
                    map.on('load', showExistsFeatures);
                    map.on('load', drawNewFeatures);
                })

                function drawNewFeatures() {
                    mapboxService.addSource(NEW_GEOMETRIES_SOURCE_NAME, map);
                    mapboxService.addPointLayer(NEW_GEOMETRIES_POINTS_LAYER_NAME, NEW_GEOMETRIES_SOURCE_NAME, map, CIRCLE_RADIUS, CIRCLE_COLOR);
                    mapboxService.addLineStringLayer(NEW_GEOMETRIES_LINES_LAYER_NAME, NEW_GEOMETRIES_SOURCE_NAME, map, LINE_COLOR, LINE_WIDTH);
                    mapboxService.addFillLayer(NEW_GEOMETRIES_FILL_LAYER_NAME, NEW_GEOMETRIES_SOURCE_NAME, map, FILL_COLOR, FILL_OPACITY);
                    map.on('click', drawFeature)
                }

                function showExistsFeatures() {
                    mapboxService.addSource(EXISTS_GEOMETRIES_SOURCE_NAME, map);
                    mapboxService.addLineStringLayer(EXISTS_GEOMETRIES_LINES_LAYER_NAME, EXISTS_GEOMETRIES_SOURCE_NAME, map, ['get', 'color'], LINE_WIDTH);
                    mapboxService.addFillLayer(EXISTS_GEOMETRIES_FILL_LAYER_NAME, EXISTS_GEOMETRIES_SOURCE_NAME, map, FILL_COLOR, FILL_OPACITY);
                    mapboxService.addMarkerLayer(EXISTS_GEOMETRIES_MARKER_LAYER_NAME, EXISTS_GEOMETRIES_SOURCE_NAME, ['get', 'id'], map)
                    backupInfoService.getAll().then(backupInfos => {
                        $scope.backupInfos = backupInfos;
                    })
                    mapboxService.getAll().then(featureCollection => {
                        existsFeatureCollection.features.push(...featureCollection.features);
                        drawGeometry(EXISTS_GEOMETRIES_SOURCE_NAME, existsFeatureCollection, map)
                    })
                    map.on('contextmenu', showInfo)
                }

                function showInfo(e) {
                    $scope.info = [];
                    if (!marker) {
                        marker = mapboxService.createMarker(e, map);
                    } else {
                        marker = marker.setLngLat(e.lngLat)
                    }
                    let features = map.queryRenderedFeatures(e.point, {
                        layers: [EXISTS_GEOMETRIES_LINES_LAYER_NAME, EXISTS_GEOMETRIES_FILL_LAYER_NAME]
                    });
                    features.map(feature => {
                        if (feature.layer.type === POLYGON_LAYER_TYPE_NAME && feature.properties.square) {
                            $scope.info.push(`id: ${feature.properties.id}, square: ${feature.properties.square.toFixed(2)} m2.`);
                        }
                        if (feature.layer.type === SEGMENT_LAYER_TYPE_NAME && feature.properties.length) {
                            $scope.info.push(`id: ${feature.properties.id}, length: ${feature.properties.length.toFixed(2)} m.`);
                        }
                    })
                    $scope.$apply();
                }

                function drawFeature(e) {
                    if ($scope.currentDrawingMode === $scope.drawingModes.SEGMENT) {
                        if (featureCollection.features.length === 0) {
                            let start = {
                                lon: e.lngLat.lng,
                                lat: e.lngLat.lat
                            }
                            addPoint(start, map);
                            return;
                        }
                        if (featureCollection.features.length === 1) {
                            let end = {
                                lon: e.lngLat.lng,
                                lat: e.lngLat.lat
                            }
                            addPoint(end, map);
                            addLine(map)
                            return;
                        }
                    }
                    if ($scope.currentDrawingMode === $scope.drawingModes.POLYGON) {
                        let point = {
                            lon: e.lngLat.lng,
                            lat: e.lngLat.lat
                        }
                        addPoint(point, map);
                        return;
                    }
                }

                function addPoint(coordinate, map) {
                    featureCollection.features.push(segmentService.createPoint(coordinate));
                    drawGeometry(NEW_GEOMETRIES_SOURCE_NAME, featureCollection, map)
                }

                function addLine(map) {
                    if (featureCollection.features) {
                        let start = featureCollection.features[START_POINT_INDEX].geometry.coordinates;
                        let end = featureCollection.features[END_POINT_INDEX].geometry.coordinates;
                        featureCollection.features.push(segmentService.createLine(start, end));
                        drawGeometry(NEW_GEOMETRIES_SOURCE_NAME, featureCollection, map)
                    }
                }

                function drawGeometry(sourceName, data, map) {
                    map.getSource(sourceName).setData(data);
                }

                $scope.onSaveSegment = function () {
                    if (featureCollection.features.length > SEGMENT_COUNT) {
                        segmentService.saveSegment([featureCollection.features[START_POINT_INDEX].geometry.coordinates, featureCollection.features[END_POINT_INDEX].geometry.coordinates])
                            .then(
                                response => {
                                    alert("segment was saved!");
                                    existsFeatureCollection.features.push(...featureCollection.features);
                                    drawGeometry(EXISTS_GEOMETRIES_SOURCE_NAME, existsFeatureCollection, map);
                                    clearGeometry(NEW_GEOMETRIES_SOURCE_NAME);
                                },
                                error => {
                                    alert("segment was not saved");
                                    clearGeometry(NEW_GEOMETRIES_SOURCE_NAME);
                                }
                            );
                    }
                }

                $scope.onSavePolygon = function () {
                    if (featureCollection.features.length) {
                        featureCollection.features.push(featureCollection.features[START_POINT_INDEX])
                        let coordinates = getPolygonCoordinates(featureCollection);
                        polygonService.save(coordinates)
                            .then(
                                response => {
                                    alert("polygon was saved!");
                                    existsFeatureCollection.features.push(...featureCollection.features);
                                    drawGeometry(EXISTS_GEOMETRIES_SOURCE_NAME, existsFeatureCollection, map);
                                    clearGeometry(NEW_GEOMETRIES_SOURCE_NAME);
                                },
                                error => {
                                    alert("polygon was not saved");
                                    clearGeometry(NEW_GEOMETRIES_SOURCE_NAME);
                                }
                            );
                    }
                }

                $scope.onCancelSegment = function () {
                    clearGeometry(NEW_GEOMETRIES_SOURCE_NAME);
                }

                $scope.onDrawPolygon = function () {
                    let polygon = {
                        coordinates: []
                    };
                    featureCollection.features.map(feature => {
                        polygon.coordinates.push({
                            lon: feature.geometry.coordinates[0],
                            lat: feature.geometry.coordinates[1]
                        });
                    });
                    featureCollection.features.push(polygonService.createPolygonFeature(polygon));
                    drawGeometry(NEW_GEOMETRIES_SOURCE_NAME, featureCollection, map);
                }

                $scope.changeLayer = function () {
                    switchLayer(map);
                }

                $scope.filterByDate = function () {
                    let date = $scope.backupDate.toLocaleDateString("fr-CA"); // "fr-CA" converts to yyyy-MM-dd date format
                    backupInfoService.getAllByDate(date).then(
                        response => {
                            updateBackupInfo(response);
                        },
                        error => {
                            alert("Can`t filter backup info!")
                        }
                    )
                };

                $scope.backup = function () {
                    mapboxService.backup().then(
                        response => {
                            alert("data was backed up!");
                            updateBackupData();
                        },
                        error => {
                            alert("data was not backed up");
                        }
                    )
                };

                $scope.restore = function () {
                    if ($scope.currentBackupInfo) {
                        mapboxService.restore($scope.currentBackupInfo).then(
                            response => {
                                alert("data was restored!");
                                updateExistsGeometries();
                                backupInfoService.getAll().then(backups => updateBackupInfo(backups));
                            },
                            error => {
                                alert("data was not restored");
                            }
                        )
                    }
                };

                function updateBackupInfo(backupInfos) {
                    $scope.backupInfos = backupInfos;
                }

                function updateBackupData() {
                    backupInfoService.getAll().then(backups => updateBackupInfo(backups));
                }

                function clearGeometry(name) {
                    featureCollection.features = [];
                    drawGeometry(name, featureCollection, map)
                }

                function updateExistsGeometries() {
                    clearGeometry(EXISTS_GEOMETRIES_SOURCE_NAME);
                    clearGeometry(NEW_GEOMETRIES_SOURCE_NAME);
                    mapboxService.getAll().then(featureCollection => {
                        existsFeatureCollection.features = featureCollection.features;
                        drawGeometry(EXISTS_GEOMETRIES_SOURCE_NAME, existsFeatureCollection, map)
                    })
                }

                function switchLayer(map) {
                    map.setStyle(STYLE_ID);
                }

                function getPolygonCoordinates(featureCollection) {
                    let coordinates = [];
                    featureCollection.features
                        .filter(feature => feature.geometry.type === 'Point')
                        .map(feature => {
                            coordinates.push([feature.geometry.coordinates[0], feature.geometry.coordinates[1]])
                        });
                    return [coordinates];
                }
            }]);