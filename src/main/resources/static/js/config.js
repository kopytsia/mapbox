angular
    .module('app', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/mapbox.html',
                controller: 'mapboxController',
                controllerAs: 'mapbox'
            })
    }]);