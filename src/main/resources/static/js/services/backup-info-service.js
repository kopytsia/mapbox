angular.module('app')
    .service('backupInfoService', function ($http) {
        this.getAll = function () {
            return $http.get('api/backup/info').then(response => response.data);
        }

        this.getAllByDate = function (date) {
            return $http.get(`api/backup/info/${date}`).then(response => response.data);
        }
    });