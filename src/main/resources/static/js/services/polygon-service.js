angular.module('app')
    .service('polygonService', function ($http) {
        this.createPolygonFeature = function (polygon) {
            return {
                type: 'Feature',
                geometry: {
                    type: 'Polygon',
                    coordinates: [polygon.coordinates.map(coordinate => [[coordinate.lon, coordinate.lat]])]
                }
            };
        }

        this.save = function (coordinates) {
            return $http.post('api/polygon/save', coordinates);
        }
    })