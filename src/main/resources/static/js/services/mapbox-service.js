angular.module('app')
    .service('mapboxService', function ($http) {
        const DEFAULT_CENTER_COORDINATES = [2.3399, 48.8555];
        const DEFAULT_ZOOM = 12;

        this.createMarker = function (e, map) {
            return new mapboxgl.Marker()
                .setLngLat(e.lngLat)
                .addTo(map);
        }

        this.getToken = function () {
            return $http.get('api/mapbox/token');
        }

        this.createMap = function (token) {
            mapboxgl.accessToken = token;
            return new mapboxgl.Map({
                container: 'map',
                style: 'mapbox://styles/mapbox/streets-v11',
                center: DEFAULT_CENTER_COORDINATES,
                zoom: DEFAULT_ZOOM
            });
        }

        this.addSource = function (name, map) {
            map.addSource(name, {
                'type': 'geojson',
                'data': {
                    'type': 'FeatureCollection',
                    'features': []
                }
            });
        }

        this.addPointLayer = function (layerName, sourceName, map, circleRadius, circleColor) {
            map.addLayer({
                id: layerName,
                type: 'circle',
                source: sourceName,
                paint: {
                    'circle-radius': circleRadius,
                    'circle-color': circleColor
                },
                filter: ['in', '$type', 'Point']
            });
        }

        this.addLineStringLayer = function (layerName, sourceName, map, color, lineWidth) {
            map.addLayer({
                id: layerName,
                type: 'line',
                source: sourceName,
                layout: {
                    'line-cap': 'round',
                    'line-join': 'round'
                },
                paint: {
                    'line-color': color,
                    'line-width': lineWidth
                },
                filter: ['in', '$type', 'LineString']
            });
        }

        this.addMarkerLayer = function (layerName, sourceName, textField, map) {
            map.addLayer({
                'id': layerName,
                'type': 'symbol',
                'source': sourceName,
                'layout': {
                    'text-field': textField
                }
            });
        }

        this.addFillLayer = function (layerName, sourceName, map, fillColor, fillOpacity) {
            map.addLayer({
                id: layerName,
                type: 'fill',
                source: sourceName,
                layout: {},
                paint: {
                    'fill-color': fillColor,
                    'fill-opacity': fillOpacity
                }
            });
        }

        this.getAll = function () {
            return $http.get('api/mapbox').then(response => response.data);
        }

        this.backup = function () {
            return $http.post('api/mapbox/backup');
        }

        this.restore = function (backupInfo) {
            return $http.post('api/mapbox/restore', backupInfo);
        }
    });