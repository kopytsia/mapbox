angular.module('app')
    .service('segmentService', function ($http) {
        this.saveSegment = function (coordinates) {
            return $http.post('api/segment/save', coordinates);
        }

        this.createPoint = function (coordinate) {
            return {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [coordinate.lon, coordinate.lat]
                }
            }
        }

        this.createLine = function (start, end) {
            return {
                'type': 'Feature',
                'geometry': {
                    'type': 'LineString',
                    'coordinates': [
                        [start[0], start[1]],
                        [end[0], end[1]]
                    ]
                }
            }
        }
    });