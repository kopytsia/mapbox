package utils;

import com.example.mapbox.model.MapboxFeature;
import com.example.mapbox.model.MapboxFeatureCollection;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

public class TestUtils {
    public static final double DEFAULT_COORDINATE_VALUE = 0.1;
    public static final double DEFAULT_SQUARE_VALUE = 0.1;
    public static final double DEFAULT_LENGTH_VALUE = 0.1;
    public static final long DEFAULT_ID_VALUE = 1;
    public static final int DEFAULT_SIZE_VALUE = 5;
    public static final String DEFAULT_COLOR_VALUE = "#000000";
    public static final String TEST_MAPBOX_POLYGON_FEATURE_PATH = "src/test/java/static/test_polygon_feature.json";
    public static final String TEST_MAPBOX_SEGMENT_FEATURE_PATH = "src/test/java/static/test_segment_feature.json";
    public static final String TEST_MAPBOX_FEATURE_COLLECTION_PATH = "src/test/java/static/test_mapbox_feature_collection.json";
    public static final String TEST_TOKEN = UUID.randomUUID().toString();
    public static final String TEST_POLYGON_JSON_FILENAME = "test_polygons.json";
    public static final String TEST_DIR = "src/test/java/static/";
    public static final String TEST_SEGMENT_JSON_FILENAME = "test_segments.json";
    public static final String TEST_BUCKET_NAME = "mapbox-bucket";

    @SneakyThrows
    public static JsonNode readFileAsJsonNode(String filePath) {
        return getObjectMapper().readTree(new File(filePath));
    }

    @SneakyThrows
    public static String readFileAsString(File file) {
        return Files.readString(file.toPath());
    }

    @SneakyThrows
    public static String readFileAsString(String filepath) {
        return Files.readString(Path.of(filepath));
    }

    @SneakyThrows
    public static JsonNode readMapboxFeatureAsJsonNode(MapboxFeature feature) {
        return getObjectMapper().readTree(getObjectMapper().writeValueAsString(feature));
    }

    @SneakyThrows
    public static JsonNode readMapboxFeatureCollectionAsString(MapboxFeatureCollection featureCollection) {
        return getObjectMapper().readTree(getObjectMapper().writeValueAsString(featureCollection));
    }

    public static ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }

}
