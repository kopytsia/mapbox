package com.example.mapbox.service.impl;

import com.example.mapbox.converter.PolygonConverter;
import com.example.mapbox.model.Polygon;
import com.example.mapbox.repository.PolygonRepository;
import com.example.mapbox.service.AmazonS3Service;
import com.example.mapbox.service.BackupInfoService;
import com.example.mapbox.service.PolygonService;
import generators.PolygonGenerator;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.File;
import java.util.List;
import java.util.Optional;

import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ONE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static utils.TestUtils.TEST_DIR;
import static utils.TestUtils.TEST_POLYGON_JSON_FILENAME;

public class PolygonServiceImplTest {
    private final PolygonRepository polygonRepository = Mockito.mock(PolygonRepository.class);
    private final PolygonConverter polygonConverter = Mockito.mock(PolygonConverter.class);
    private final AmazonS3Service amazonS3Service = Mockito.mock(AmazonS3Service.class);
    private final BackupInfoService backupInfoService = Mockito.mock(BackupInfoService.class);
    private final PolygonService polygonService = new PolygonServiceImpl(polygonRepository, polygonConverter, amazonS3Service, backupInfoService);

    @Test
    void saveTest() {
        List<List<List<Double>>> coordinates = PolygonGenerator.generatePolygon().getCoordinates();
        when(polygonRepository.save(coordinates)).thenReturn(INTEGER_ONE);
        boolean actual = polygonService.save(coordinates);
        assertThat(actual, is(true));
    }

    @Test
    void getAllTest() {
        List<Polygon> expected = PolygonGenerator.generatePolygons();
        when(polygonRepository.findAll()).thenReturn(expected);
        List<Polygon> actual = polygonService.getAll();
        assertThat(actual, is(expected));
    }

    @SneakyThrows
    @Test
    void backupTest() {
        List<Polygon> polygons = PolygonGenerator.generatePolygons();
        when(polygonRepository.findAll()).thenReturn(polygons);
        File file = new File(TEST_DIR + TEST_POLYGON_JSON_FILENAME);
        when(polygonConverter.toFile(anyString(), anyString(), any())).thenReturn(file);
        when(backupInfoService.create(anyString(), any())).thenReturn(true);
        boolean actual = polygonService.backup(TEST_POLYGON_JSON_FILENAME);
        assertThat(actual, is(true));
    }

    @SneakyThrows
    @Test
    void restoreTest() {
        Optional<File> file = Optional.of(new File(TEST_DIR + TEST_POLYGON_JSON_FILENAME));
        List<Polygon> polygons = PolygonGenerator.generatePolygons();
        when(amazonS3Service.download(anyString())).thenReturn(file);
        when(polygonConverter.fromFile(eq(file.get()))).thenReturn(polygons);
        when(polygonRepository.saveAll(any())).thenReturn(INTEGER_ONE);
        boolean actual = polygonService.restore(TEST_POLYGON_JSON_FILENAME);
        assertThat(actual, is(true));
    }
}