package com.example.mapbox.service.impl;

import com.example.mapbox.client.MapboxClient;
import com.example.mapbox.config.MapboxConfig;
import com.example.mapbox.converter.MapboxConverter;
import com.example.mapbox.converter.PolygonConverter;
import com.example.mapbox.converter.SegmentConverter;
import com.example.mapbox.model.BackupInfo;
import com.example.mapbox.model.MapboxToken;
import com.example.mapbox.model.Polygon;
import com.example.mapbox.model.Segment;
import com.example.mapbox.service.MapboxService;
import com.example.mapbox.service.PolygonService;
import com.example.mapbox.service.SegmentService;
import com.fasterxml.jackson.databind.JsonNode;
import generators.BackupInfoGenerator;
import generators.MapboxFeatureGenerator;
import generators.PolygonGenerator;
import generators.SegmentGenerator;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import utils.TestUtils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static utils.TestUtils.*;

public class MapboxServiceImplTest {
    private final SegmentService segmentService = Mockito.mock(SegmentService.class);
    private final PolygonService polygonService = Mockito.mock(PolygonService.class);
    private final PolygonConverter polygonConverter = Mockito.mock(PolygonConverter.class);
    private final SegmentConverter segmentConverter = Mockito.mock(SegmentConverter.class);
    private final MapboxConverter mapboxConverter = Mockito.mock(MapboxConverter.class);
    private final MapboxClient mapboxClient = Mockito.mock(MapboxClient.class);
    private final MapboxConfig mapboxConfig = Mockito.mock(MapboxConfig.class);
    private final MapboxService mapboxService = new MapboxServiceImpl(
            segmentService, polygonService, polygonConverter, segmentConverter, mapboxConverter, mapboxClient, mapboxConfig);

    @Test
    void getTokenTest() {
        when(mapboxConfig.getMapboxToken()).thenReturn(TEST_TOKEN);
        MapboxToken actual = mapboxService.getToken();
        assertThat(actual.getToken(), is(TEST_TOKEN));
    }

    @SneakyThrows
    @Test
    void getFeatureCollectionTest() {
        when(segmentService.getAll()).thenReturn(SegmentGenerator.generateSegments());
        when(polygonService.getAll()).thenReturn(PolygonGenerator.generatePolygons());
        when(segmentConverter.toMapboxFeature(any(Segment.class)))
                .thenReturn(MapboxFeatureGenerator.generateSegmentMapboxFeature());
        when(polygonConverter.toMapboxFeature(any(Polygon.class)))
                .thenReturn(MapboxFeatureGenerator.generatePolygonMapboxFeature());
        JsonNode expected = TestUtils.readFileAsJsonNode(TEST_MAPBOX_FEATURE_COLLECTION_PATH);
        JsonNode actual = TestUtils.readMapboxFeatureCollectionAsString(mapboxService.getFeatureCollection());
        assertThat(actual, is(expected));
    }

    @SneakyThrows
    @Test
    void backupGeometriesTest() {
        when(polygonService.backup(anyString())).thenReturn(true);
        when(segmentService.backup(anyString())).thenReturn(true);
        boolean actual = mapboxService.backupGeometries();
        assertThat(actual, is(true));
    }

    @SneakyThrows
    @Test
    void restoreGeometriesTest() {
        when(segmentService.restore(anyString())).thenReturn(true);
        BackupInfo backupInfo = BackupInfoGenerator.generateBackupInfo();
        boolean actual = mapboxService.restoreGeometries(backupInfo);
        assertThat(actual, is(true));
    }
}