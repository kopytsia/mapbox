package com.example.mapbox.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.example.mapbox.service.AmazonS3Service;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.io.File;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static utils.TestUtils.*;

class AmazonS3ServiceImplTest {
    private final AmazonS3 amazonS3 = Mockito.mock(AmazonS3.class);
    private final AmazonS3Service amazonS3Service = new AmazonS3ServiceImpl(amazonS3);
    private final ArgumentCaptor<GetObjectRequest> getObjectRequestArgumentCaptor = ArgumentCaptor.forClass(GetObjectRequest.class);


    @Test
    void uploadTest() {
        File expectedFile = new File(TEST_DIR + TEST_SEGMENT_JSON_FILENAME);
        amazonS3Service.upload(TEST_SEGMENT_JSON_FILENAME, expectedFile);
        verify(amazonS3).putObject(TEST_BUCKET_NAME, TEST_SEGMENT_JSON_FILENAME, expectedFile);
    }

    @SneakyThrows
    @Test
    void downloadTest() {
        Optional<File> actual = amazonS3Service.download(TEST_SEGMENT_JSON_FILENAME);
        assertThat(actual.isPresent(), is(true));
        verify(amazonS3).getObject(getObjectRequestArgumentCaptor.capture(), any(File.class));
        GetObjectRequest actualParams = getObjectRequestArgumentCaptor.getValue();
        assertThat(actualParams.getBucketName(), is(TEST_BUCKET_NAME));
        assertThat(actualParams.getKey(), is(TEST_SEGMENT_JSON_FILENAME));
    }
}