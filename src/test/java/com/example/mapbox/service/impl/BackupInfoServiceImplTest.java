package com.example.mapbox.service.impl;

import com.example.mapbox.model.BackupGeometryType;
import com.example.mapbox.model.BackupInfo;
import com.example.mapbox.repository.BackupInfoRedisRepository;
import com.example.mapbox.service.BackupInfoService;
import generators.BackupInfoGenerator;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import utils.TestUtils;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

class BackupInfoServiceImplTest {
    private final BackupInfoRedisRepository backupInfoRepository = Mockito.mock(BackupInfoRedisRepository.class);
    private final BackupInfoService backupInfoService = new BackupInfoServiceImpl(backupInfoRepository);

    @Test
    void createTest() {
        boolean actual = backupInfoService.create(TestUtils.TEST_POLYGON_JSON_FILENAME, BackupGeometryType.POLYGON);
        assertThat(actual, is(true));
    }

    @Test
    void getAllTest() {
        List<BackupInfo> expected = BackupInfoGenerator.generateBackupInfos();
        when(backupInfoRepository.findAll()).thenReturn(expected);
        List<BackupInfo> actual = backupInfoService.getAll();
        assertThat(actual, is(expected));
    }
}