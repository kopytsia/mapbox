package com.example.mapbox.service.impl;

import com.example.mapbox.converter.SegmentConverter;
import com.example.mapbox.model.Segment;
import com.example.mapbox.repository.SegmentRepository;
import com.example.mapbox.repository.impl.SegmentRepositoryImpl;
import com.example.mapbox.service.AmazonS3Service;
import com.example.mapbox.service.BackupInfoService;
import com.example.mapbox.service.SegmentService;
import generators.SegmentGenerator;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;
import java.util.Optional;

import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ONE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static utils.TestUtils.TEST_DIR;
import static utils.TestUtils.TEST_SEGMENT_JSON_FILENAME;

public class SegmentServiceImplTest {
    private final SegmentRepository segmentRepository = mock(SegmentRepositoryImpl.class);
    private final SegmentConverter segmentConverter = mock(SegmentConverter.class);
    private final AmazonS3Service amazonS3Service = mock(AmazonS3Service.class);
    private final BackupInfoService backupInfoService = mock(BackupInfoService.class);
    private final SegmentService segmentService = new SegmentServiceImpl(segmentRepository, segmentConverter, amazonS3Service, backupInfoService);

    @Test
    void saveTest() {
        List<List<Double>> coordinates = SegmentGenerator.generateSegment().getCoordinates();
        when(segmentRepository.save(coordinates)).thenReturn(INTEGER_ONE);
        boolean actual = segmentService.save(coordinates);
        assertThat(actual, is(true));
    }

    @Test
    void getAllTest() {
        List<Segment> expected = SegmentGenerator.generateSegments();
        when(segmentRepository.findAll()).thenReturn(expected);
        List<Segment> actual = segmentService.getAll();
        assertThat(actual, is(expected));
    }

    @SneakyThrows
    @Test
    void backupTest() {
        List<Segment> segments = SegmentGenerator.generateSegments();
        when(segmentRepository.findAll()).thenReturn(segments);
        File file = new File(TEST_DIR + TEST_SEGMENT_JSON_FILENAME);
        when(segmentConverter.toFile(anyString(), anyString(), any())).thenReturn(file);
        when(backupInfoService.create(anyString(), any())).thenReturn(true);
        boolean actual = segmentService.backup(TEST_SEGMENT_JSON_FILENAME);
        assertThat(actual, is(true));
    }

    @SneakyThrows
    @Test
    void restoreTest() {
        Optional<File> fileOptional = Optional.of(new File(TEST_DIR + TEST_SEGMENT_JSON_FILENAME));
        List<Segment> segments = SegmentGenerator.generateSegments();
        when(amazonS3Service.download(anyString())).thenReturn(fileOptional);
        when(segmentConverter.fromFile(eq(fileOptional.get()))).thenReturn(segments);
        when(segmentRepository.saveAll(any())).thenReturn(INTEGER_ONE);
        boolean actual = segmentService.restore(TEST_SEGMENT_JSON_FILENAME);
        assertThat(actual, is(true));
    }
}