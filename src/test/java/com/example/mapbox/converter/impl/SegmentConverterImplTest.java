package com.example.mapbox.converter.impl;

import com.example.mapbox.converter.SegmentConverter;
import com.example.mapbox.model.Segment;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import generators.SegmentGenerator;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import utils.TestUtils;

import java.io.File;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static utils.TestUtils.*;

public class SegmentConverterImplTest {
    private final SegmentConverter segmentConverter = new SegmentConverterImpl(TestUtils.getObjectMapper());

    @Test
    void toMapboxFeatureTest() {
        Segment segment = SegmentGenerator.generateSegment();
        JsonNode expected = TestUtils.readFileAsJsonNode(TEST_MAPBOX_SEGMENT_FEATURE_PATH);
        JsonNode actual = TestUtils.readMapboxFeatureAsJsonNode(segmentConverter.toMapboxFeature(segment));
        assertThat(actual, is(expected));
    }

    @SneakyThrows
    @Test
    void toFileTest() {
        List<Segment> segments = SegmentGenerator.generateSegments();
        String expected = readFileAsString(TEST_DIR + TEST_SEGMENT_JSON_FILENAME);
        String actual = readFileAsString(segmentConverter.toFile(TEST_SEGMENT_JSON_FILENAME, ".json", segments));
        assertThat(actual, is(expected));
    }

    @SneakyThrows
    @Test
    void fromFileTest() {
        File file = new File(TEST_DIR + TEST_SEGMENT_JSON_FILENAME);
        List<Segment> expected = SegmentGenerator.generateSegments();
        List<Segment> actual = segmentConverter.fromFile(file);
        assertThat(actual, is(expected));
    }
}