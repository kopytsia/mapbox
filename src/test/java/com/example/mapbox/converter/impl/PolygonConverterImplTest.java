package com.example.mapbox.converter.impl;

import com.example.mapbox.converter.PolygonConverter;
import com.example.mapbox.model.Polygon;
import com.fasterxml.jackson.databind.JsonNode;
import generators.PolygonGenerator;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import utils.TestUtils;

import java.io.File;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static utils.TestUtils.*;

public class PolygonConverterImplTest {
    private final PolygonConverter polygonConverter = new PolygonConverterImpl(TestUtils.getObjectMapper());

    @Test
    void toMapboxFeatureTest() {
        Polygon polygon = PolygonGenerator.generatePolygon();
        JsonNode expected = TestUtils.readFileAsJsonNode(TEST_MAPBOX_POLYGON_FEATURE_PATH);
        JsonNode actual = TestUtils.readMapboxFeatureAsJsonNode(polygonConverter.toMapboxFeature(polygon));
        assertThat(actual, is(expected));
    }

    @Test
    @SneakyThrows
    void toFileTest() {
        List<Polygon> polygons = PolygonGenerator.generatePolygons();
        String expected = readFileAsString(TEST_DIR + TEST_POLYGON_JSON_FILENAME);
        String actual = readFileAsString(polygonConverter.toFile(TEST_POLYGON_JSON_FILENAME, ".json", polygons));
        assertThat(actual, is(expected));
    }

    @Test
    @SneakyThrows
    void fromFileTest() {
        File file = new File(TEST_DIR + TEST_POLYGON_JSON_FILENAME);
        List<Polygon> expected = PolygonGenerator.generatePolygons();
        List<Polygon> actual = polygonConverter.fromFile(file);
        assertThat(actual, is(expected));
    }
}