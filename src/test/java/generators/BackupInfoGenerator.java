package generators;

import com.example.mapbox.model.BackupGeometryType;
import com.example.mapbox.model.BackupInfo;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;
import static utils.TestUtils.*;

public class BackupInfoGenerator {
    public static BackupInfo generateBackupInfo() {
        return new BackupInfo(
                LocalDateTime.now(),
                TEST_SEGMENT_JSON_FILENAME,
                BackupGeometryType.SEGMENT
        );
    }

    public static List<BackupInfo> generateBackupInfos() {
        return IntStream.range(INTEGER_ZERO, DEFAULT_SIZE_VALUE)
                .mapToObj(i -> generateBackupInfo())
                .collect(Collectors.toUnmodifiableList());
    }
}
