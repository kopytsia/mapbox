package generators;

import com.example.mapbox.model.Polygon;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;
import static utils.TestUtils.*;

public class PolygonGenerator {
    public static Polygon generatePolygon() {
        return new Polygon(
                DEFAULT_ID_VALUE,
                DEFAULT_SQUARE_VALUE,
                List.of(
                        List.of(
                                List.of(DEFAULT_COORDINATE_VALUE, DEFAULT_COORDINATE_VALUE),
                                List.of(DEFAULT_COORDINATE_VALUE, DEFAULT_COORDINATE_VALUE)
                        )
                )
        );
    }

    public static List<Polygon> generatePolygons() {
        return IntStream.range(INTEGER_ZERO, DEFAULT_SIZE_VALUE)
                .mapToObj(i -> generatePolygon())
                .collect(Collectors.toUnmodifiableList());
    }
}
