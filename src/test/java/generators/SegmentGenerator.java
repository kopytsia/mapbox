package generators;

import com.example.mapbox.model.Segment;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;
import static org.apache.commons.lang3.math.NumberUtils.LONG_ONE;
import static utils.TestUtils.*;

public class SegmentGenerator {
    public static List<Segment> generateSegments() {
        return IntStream.range(INTEGER_ZERO, DEFAULT_SIZE_VALUE)
                .mapToObj(i -> generateSegment())
                .collect(Collectors.toUnmodifiableList());
    }

    public static Segment generateSegment() {
        return new Segment(
                LONG_ONE,
                DEFAULT_LENGTH_VALUE,
                List.of(
                        List.of(DEFAULT_COORDINATE_VALUE, DEFAULT_COORDINATE_VALUE),
                        List.of(DEFAULT_COORDINATE_VALUE, DEFAULT_COORDINATE_VALUE)
                ),
                DEFAULT_COLOR_VALUE
        );
    }
}
