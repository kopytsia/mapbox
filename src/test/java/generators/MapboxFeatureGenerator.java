package generators;

import com.example.mapbox.model.MapboxFeature;
import com.example.mapbox.model.PolygonMapboxGeometry;
import com.example.mapbox.model.SegmentMapboxGeometry;

import java.util.Map;

import static utils.TestUtils.*;

public class MapboxFeatureGenerator {
    public static MapboxFeature generateSegmentMapboxFeature() {
        Map<String, Object> properties = Map.of(
                "id", DEFAULT_ID_VALUE,
                "length", DEFAULT_LENGTH_VALUE,
                "color", DEFAULT_COLOR_VALUE
        );
        return new MapboxFeature(
                new SegmentMapboxGeometry(SegmentGenerator.generateSegment().getCoordinates()),
                properties
        );
    }

    public static MapboxFeature generatePolygonMapboxFeature() {
        Map<String, Object> properties = Map.of(
                "id", DEFAULT_ID_VALUE,
                "square", DEFAULT_SQUARE_VALUE
        );
        return new MapboxFeature(
                new PolygonMapboxGeometry(PolygonGenerator.generatePolygon().getCoordinates()),
                properties
        );
    }
}
