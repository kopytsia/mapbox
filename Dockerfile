FROM openjdk:14-alpine3.10
RUN apk add maven \
&& apk add git \
&& git clone https://bitbucket.org/kopytsia/mapbox.git \
&& mvn -f mapbox/pom.xml install
WORKDIR ./mapbox
CMD java -jar target/mapbox-0.0.1-SNAPSHOT.jar
EXPOSE 8080